package com.helloworld;

import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import proto.inventa.cct.com.inventalibrary.InventaSdk;
import proto.inventa.cct.com.inventalibrary.auth.LoginAuth;
import proto.inventa.cct.com.inventalibrary.discovery.DiscoveryHelper;
import proto.inventa.cct.com.inventalibrary.exceptions.APIException;
import proto.inventa.cct.com.inventalibrary.profile.ProfileHelper;
import proto.inventa.cct.com.inventalibrary.rest.LoginAPIHelper;
import proto.inventa.cct.com.inventalibrary.rest.SubscriptionHelper;
import proto.inventa.cct.com.inventalibrary.store.StoreHelper;

public class ReactInventaInitModule extends ReactContextBaseJavaModule {


    ReactApplicationContext reactApplicationContext;

    public ReactInventaInitModule(@NonNull ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactApplicationContext = reactContext;
    }

    @ReactMethod
    public boolean initialiseInventaSDK(boolean isLoggedIn, Callback errorCallback, Callback successCallback) {
        Toast.makeText(reactApplicationContext, "Hello", Toast.LENGTH_SHORT).show();
        InventaSdk.initializeInventaSdk(reactApplicationContext);
        DiscoveryHelper.setDiscoveryTransitionsReceiver(new DiscoveryBroadcastReceiver());
        successCallback.invoke("SUCCESS");
        return false;
    }

    @NonNull
    @Override
    public String getName() {
        return "ReactInventaInitModule";
    }
}
