package com.helloworld;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import proto.inventa.cct.com.inventalibrary.discovery.DiscoveryHelper;
import proto.inventa.cct.com.inventalibrary.notification.NotificationHelper;

public class DiscoveryBroadcastReceiver extends BroadcastReceiver {

    public static final String LOG_TAG = DiscoveryBroadcastReceiver.class.getSimpleName() + " InventaNotification ";
    @Override
    public void onReceive(Context context, Intent intent) {

        String action = intent.getAction();

        if (DiscoveryHelper.ACTION_DISCOVERY_GEOZONES_BROADCAST.equals(action)) {
        } else if (DiscoveryHelper.ACION_DISCOVERY_ENGAGEMENTZONES_BROADCAST.equals(action)) {
        } else if (DiscoveryHelper.ACION_DISCOVERY_INSTORE_BROADCAST.equals(action)) {
        } else if (NotificationHelper.ACTION_NOTIFICATION_CLICK_GZONE.equals(action)) {
            Log.d(LOG_TAG, "  ACTION_NOTIFICATION_CLICK_GZONE ");
        } else if (NotificationHelper.ACTION_NOTIFICATION_CLICK_EZONE.equals(action)) {
            Log.d(LOG_TAG, "  ACTION_NOTIFICATION_CLICK_EZONE ");
        } else if (NotificationHelper.ACTION_NOTIFICATION_CLICK_INSTORE.equals(action)) {
            Log.d(LOG_TAG, "  ACTION_NOTIFICATION_CLICK_INSTORE ");
        } else if (NotificationHelper.ACTION_NOTIFICATION_CLICK_BROADCAST_GZONE.equals(action)) {
            Log.d(LOG_TAG, "  ACTION_NOTIFICATION_CLICK_BROADCAST_GZONE ");
        } else if (NotificationHelper.ACTION_NOTIFICATION_CLICK_BROADCAST_EZONE.equals(action)) {
            Log.d(LOG_TAG, "  ACTION_NOTIFICATION_CLICK_BROADCAST_EZONE ");
        }
    }
}
