import ReactInventaInitModule from './InventaDemo';
import App from "./App";
ReactInventaInitModule.initialiseInventaSDK(false,
    (msg) => {
        console.error(msg)
    },
    (someData) => {
        console.log(someData)
    })

export default App;